package com.assignment.sendmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildApiApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildApiApiApplication.class, args);
    }

}
