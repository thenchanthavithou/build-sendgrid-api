package com.assignment.sendmail.controller;

import com.assignment.sendmail.provider.MailProvider;
import com.assignment.sendmail.request.SendMailRequest;
import com.sendgrid.Response;
import com.sendgrid.helpers.mail.objects.Content;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Set;


@RestController
@RequiredArgsConstructor
@Slf4j
public class MailController {
    private final MailProvider emailService;

    @PostMapping("/send-email")
    public ResponseEntity<Response> send(@RequestBody SendMailRequest sendMailRequest) {
        var response = emailService.send(
                sendMailRequest.getFrom(),
                sendMailRequest.getTo(),
                sendMailRequest.getCc(),
                sendMailRequest.getSubject(),
                sendMailRequest.getContent());
        return new ResponseEntity<>(response, HttpStatus.valueOf(response.getStatusCode()));
    }

    @PostMapping("/attachments")
    public ResponseEntity<Response> sendAttachment(
            @RequestPart("file") MultipartFile file,
            @RequestPart("request") SendMailRequest sendMailRequest) {

        var response = emailService.sendAttachment(
                sendMailRequest.getFrom(),
                sendMailRequest.getTo(),
                sendMailRequest.getCc(),
                sendMailRequest.getSubject(),
                sendMailRequest.getContent(), file);
        return new ResponseEntity<>(response, HttpStatus.valueOf(response.getStatusCode()));
    }
}