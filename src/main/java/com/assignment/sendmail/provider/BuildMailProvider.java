package com.assignment.sendmail.provider;

import com.assignment.sendmail.utils.MailUtils;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
public class BuildMailProvider {
    private final SendGrid sendGrid;
    protected Mail send(String from, Set<String> to, Set<String> cc, String subject, Content content) {
        Personalization personalization = new Personalization();
        if (Objects.nonNull(to)) {
            to.forEach(eachTo -> personalization.addTo(new Email(eachTo)));
            System.out.println(to);
        }

        if (Objects.nonNull(cc)) {
            cc.forEach(eachCC -> personalization.addCc((new Email(eachCC))));
            System.out.println(cc);
        }
        Mail mail = new Mail();
        mail.setFrom(new Email(from));
        mail.addPersonalization(personalization);
        mail.setSubject(MailUtils.subjectProvider(subject));
        mail.addContent(MailUtils.contentProvider(content));
        return mail;
    }

    protected Mail sendAttachment(
            String from, Set<String> to,
            Set<String> cc, String subject,
            Content content, MultipartFile file) {
        Mail mail = send(from, to, cc, subject, content);
        try {
            Attachments attachments = new Attachments
                    .Builder(file.getOriginalFilename(), file.getInputStream())
                    .withType(file.getContentType())
                    .build();
            mail.addAttachments(attachments);
        } catch (IOException exception) {
            log.info("", exception);
        }
        return mail;
    }

    protected Response response(Mail mail) {
        Response response = new Response();
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        try {
            request.setBody(mail.build());
            response = sendGrid.api(request);
        } catch (IOException exception) {
            log.error("", exception);
        }
        return response;
    }
}
