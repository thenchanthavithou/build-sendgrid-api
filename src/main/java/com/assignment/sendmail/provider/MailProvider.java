package com.assignment.sendmail.provider;

import com.sendgrid.Response;
import com.sendgrid.helpers.mail.objects.Content;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
public class MailProvider {
    private final BuildMailProvider mailProvider;
    public Response send(String from, Set<String> to, Set<String> cc, String subject, Content content) {
        return mailProvider.response(mailProvider.send(from, to, cc, subject, content));
    }

    public Response sendAttachment(String from, Set<String> to, Set<String> cc, String subject, Content content, MultipartFile file) {

        return mailProvider.response(mailProvider.sendAttachment(from, to, cc, subject, content, file));
    }
}
