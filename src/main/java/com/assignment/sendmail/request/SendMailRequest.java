package com.assignment.sendmail.request;

import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
public class SendMailRequest {
    private String from;
    private Set<String> to;

    private Set<String> cc;

    private String subject;
    private Content content;

    private String attachmentPath;
}
