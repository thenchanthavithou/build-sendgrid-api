package com.assignment.sendmail.utils;

import com.sendgrid.helpers.mail.objects.Content;

import java.util.Objects;

public class MailUtils {
    public static Content contentProvider(Content content) {
        return Objects.isNull(content) ? new Content("text/plain", "no content") : content;
    }
    public static String subjectProvider(String subject) {
        return Objects.isNull(subject) ? "no subject" : subject;
    }
}
